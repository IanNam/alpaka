from pymongo import MongoClient


def connect(mongo_url, dbName):
    client = MongoClient(mongo_url)
    return client[dbName]


def insert(collection, document):
    collection.insert(document)


def insert_many(collection, documents):
    collection.insertMany(documents)


def update_id(column, document):
    target = document[column]
    document['_id'] = target
    return document
