import time
import blockchain_api as API
import mongo_connector as Mongo
import threading
import datetime


def get_timemilies(d):
    millis = stdt + datetime.timedelta(days=d * -1)
    print(millis)
    return int(millis.timestamp()*1000)


def get_latest_block():
    doc = Mongo.update_id('hash', API.latest_block().json())
    try:
        Mongo.insert(db['block_info'], doc)
    except Exception as e:
        print(e)


def get_oneday_blocks():
    days = 0
    while True:
        millis = get_timemilies(days)
        blocks = API.blocks_oneday(millis).json()
        for block in blocks['blocks']:
            try:
                block = Mongo.update_id('hash', block)
                Mongo.insert(db['block_info'], block)
            except Exception as e:
                print(e)
                continue
        days = days + 1
        time.sleep(10)


stdt = datetime.datetime.now()
db = Mongo.connect('mongodb://root:root@localhost:27017/', 'blockchain')
t1 = threading.Thread(target=get_oneday_blocks())
t1.daemon = True
t1.start()

print("### End ###")