import requests


def single_block(block_hash):
    return requests.get('https://blockchain.info/rawblock/{}'.format(block_hash))


def single_transaction(tx_hash):
    return requests.get('https://blockchain.info/rawtx/{}'.format(tx_hash))


def chart_data(chart_type):
    return requests.get('https://blockchain.info/charts/{}?format=json'.format(chart_type))


def block_height(block_height):
    return requests.get('https://blockchain.info/block-height/{}?format=json'.format(block_height))


def single_address(bitcoin_address):
    return requests.get('https://blockchain.info/rawaddr/{}'.format(bitcoin_address))


def multi_address(addresses):
    return requests.get('https://blockchain.info/multiaddr?active={}'.format(addresses))


def unspent_outputs(address):
    return requests.get('https://blockchain.info/unspent?active={}'.format(address))


def balance(address):
    return requests.get('https://blockchain.info/balance?active={}'.format(address))


def latest_block():
    return requests.get('https://blockchain.info/latestblock')


def unconfirmed_transactions():
    return requests.get('https://blockchain.info/unconfirmed-transactions?format=json')


def blocks_oneday(time_in_milliseconds):

    return requests.get('https://blockchain.info/blocks/{}?format=json'.format(time_in_milliseconds))


def blocks_pool(pool_name):
    return requests.get('https://blockchain.info/blocks/{}?format=json'.format(pool_name))
