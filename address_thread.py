import threading, time
import mongo_connector as Mongo
from bs4 import BeautifulSoup
import requests


def parse_address_pages():
    try_count = 0
    page_index = 1
    while try_count <= 3:
        url = 'https://bitcoinchain.com/block_explorer/catalog/{}'.format(page_index)
        html_doc = requests.get(url).text
        soup = BeautifulSoup(html_doc, 'html.parser')
        docs = soup.select('table.table.table-hover > tbody > tr > td ')

        address = {}
        idx = 0
        for data in docs:
            if idx % 5 == 0:
                address['address'] = data.text
            elif idx % 5 == 1:
                address['hash'] = data.text
            elif idx % 5 == 2:
                address['balance'] = data.text
            elif idx % 5 == 3:
                address['received'] = data.text
            elif idx % 5 == 4:
                address['transactions'] = data.text

            idx += 1

            if idx >= 5 and idx % 5 == 0:
                try:
                    doc = Mongo.update_id('address', address)
                    print("pages: {0}, idx: {1}, try: {2}, doc: {3}".format(page_index, idx, try_count, doc))
                    Mongo.insert(db['address_info'], doc)
                    address = {}
                except Exception as e:
                    try_count += 1
                    print(e)

        page_index += 1
        time.sleep(3)


db = Mongo.connect('mongodb://root:root@localhost:27017/', 'blockchain')
t1 = threading.Thread(target=parse_address_pages())
t1.daemon = True
t1.start()

print("### End ###")